import { LitElement, html } from 'lit-element'

class PersonaFooter extends LitElement{
    static get is() {
        return 'persona-footer';
    }

    static get properties() {
        return {
        }
    }

    constructor() {
        super();
    }

    updated(changedProperties) {
        console.log('Updated', changedProperties);
    }

    render() {
        return html`
            <h5>@Persona App 2021</h5>
        `;
    }
}

customElements.define(PersonaFooter.is, PersonaFooter);