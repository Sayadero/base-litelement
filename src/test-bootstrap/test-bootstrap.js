import { LitElement, html, css } from 'lit-element'

class TestBootstrap extends LitElement{
  static get is() {
    return 'test-bootstrap';
  }

  static get styles() {
    return css`
      .redbg {
        background-color: red;
      }

      .greenbg {
        background-color: green;
      }

      .bluebg {
        background-color: blue;
      }

      .greybg {
        background-color: grey;
      }
    `;
  }

  render() {
    return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
      <h3>Test Bootstrap</h3>
      <div class="row greybg">
        <div class="col-2 col-sm-6 redbg">Col 1</div>
        <div class="col-3 col-sm-1 greenbg">Col 2</div>
        <div class="col-4 col-sm-1 bluebg">Col 3</div>
      </div>
    `;
  }
}

customElements.define(TestBootstrap.is, TestBootstrap);