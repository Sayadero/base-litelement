import { LitElement, html } from 'lit-element'
import '../persona-header/persona-header.js'
import '../persona-tabs/persona-tabs.js'
import '../persona-footer/persona-footer.js'
import '../persona-stats/persona-stats.js'

class PersonaApp extends LitElement{
  static get is() {
    return 'persona-app';
  }

  static get properties() {
    return {
      people: { type: Array },
      filteredPeople: { type: Array }
    }
  }

  constructor() {
    super();

    this.people = [];
    this.filteredPeople = [];
  }

  updated(changedProperties) {
    console.log('Updated', changedProperties);

    if (changedProperties.has('people')) {
      this.shadowRoot.querySelector('persona-stats').people = this.people;
    }
  }

  render() {
    return html`
      <persona-header></persona-header>
      <persona-tabs
        @people-changed=${this._updatedPeople}
        @filtered-people-changed=${this._updatedFilteredPeople}></persona-tabs>
      <persona-footer></persona-footer>
      <persona-stats
        .people="${this.people}"
        .filteredPeople="${this.filteredPeople}"
        @people-stats-changed=${this._updatedPeopleStats}
        @filtered-people-stats-changed=${this._updatedFilteredPeopleStats}>
      </persona-stats>
    `;
  }

  _updatedPeople(event) {
    console.log('_updatedPeople');

    const { detail: { people }} = event;

    this.people = people;
  }

  _updatedFilteredPeople(event) {
    console.log('_updatedFilteredPeople');

    const { detail: { filteredPeople }} = event;

    this.filteredPeople = filteredPeople;
  }

  _updatedPeopleStats(event) {
    console.log('_updatedPeopleStats');

    const { detail: { peopleStats }} = event;

    this.shadowRoot.querySelector('persona-tabs').peopleStats = peopleStats;
  }

  _updatedFilteredPeopleStats(event) {
    console.log('_updatedFilteredPeopleStats');

    const { detail: { filteredPeopleStats }} = event;

    this.shadowRoot.querySelector('persona-tabs').filteredPeopleStats = filteredPeopleStats;
  }
}

customElements.define(PersonaApp.is, PersonaApp);