import { LitElement, html, css } from 'lit-element'
import '../persona-dm/persona-dm.js'
import '../persona-ficha-listado/persona-ficha-listado.js'
import '../persona-form/persona-form.js'

class PersonaMain extends LitElement {
  static get is() {
    return 'persona-main';
  }

  static get styles() {
    return css`
      :host {
        all: initial
      }
    `;
  }

  static get properties() {
    return {
      people: { type: Array },
      filteredPeople: {type: Array },
      showPersonForm: { type: Boolean },
      yearsRange: { type: Number, attribute: 'years-range' }
    }
  }

  constructor() {
    super();

    this.people = [];

    this.showPersonForm = false;
    this.yearsRange = 0;
    this._filterPeople();
    this._randomImages = 5;
  }

  updated(changedProperties) {
    console.log('Updated', changedProperties);

    if (changedProperties.has('people') || changedProperties.has('yearsRange')) {
      this._filterPeople();
    }

    if (changedProperties.has('people')) {
      console.log('Longitud de people', this.people.length);

      this.dispatchEvent(new CustomEvent('people-changed', {
        bubbles: true,
        composed: true,
        detail: {
          people : this.people
        }
      }));
    }

    if (changedProperties.has('filteredPeople')) {
      console.log('Longitud de filtered people', this.filteredPeople.length);

      this.dispatchEvent(new CustomEvent('filtered-people-changed', {
        bubbles: true,
        composed: true,
        detail: {
          filteredPeople : this.filteredPeople
        }
      }));
    }
  }

  render() {
    return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous"></link>
      <persona-dm
        @retrieve-people-start=${() => this.people = []}
        @retrieve-people-ok=${({detail: people}) => this.people = people}>
      </persona-dm>
      <h2 class="text-center">Personas</h2>
      <div ?hidden=${this.people.length > 0}>
      <div class="d-flex justify-content-center mt-5"><div class="spinner-border text-primary"></div></div>
      <div class="d-flex justify-content-center mt-2"><span>Loading...</span></div>
      </div>
      <div ?hidden=${this.people.length <= 0}>
        <div class="row ${this.showPersonForm ? 'd-none' : ''}" id="peopleList">
          <div class="row ${this.filteredPeople.length > 1 ? 'row-cols-1 row-cols-sm-4' : 'row-cols-10'}">
            ${this.filteredPeople.map((person) =>
              html`<persona-ficha-listado
                id="${person.id}"
                fName="${person.name}"
                yearsInCompany="${person.yearsInCompany}"
                profile="${person.profile}"
                .photo="${person.photo}"
                @info-person=${this._infoPerson}
                @delete-person=${this._deletePerson}>
              </persona-ficha-listado>`)}
          </div>
        </div>
        <div class="row ${!this.showPersonForm ? 'd-none' : ''}">
          <persona-form
            id="personForm"
            class="border rounded border-primary"
            @edit-person=${this._editPerson}
            @store-person=${this._storePerson}
            @persona-form-close=${this._personFormClose}>
          </persona-form>
        </div>
      </div>
    `;
  }

  _editPerson(event) {
    console.log('_editPerson');

    let { detail: { person }} = event;

    const index = this.people.findIndex(arrayPerson => arrayPerson.id === person.id);
    this.people[index] = { ...this.people[index], ...person };
    this.people = [ ...this.people ];

    this._closeForm();
  }

  _storePerson(event) {
    console.log('_storePerson');

    let { detail: person } = event;

    person.id = this.people.reduce((id, person) => id <= person.id ? person.id + 1 : id , 0);
    person.photo = {
      src: `./img/random/rand-${Math.floor(Math.random() * this._randomImages) + 1}.png`,
      alt: person.name
    }

    this.people = [ ...this.people, person];

    this._closeForm();
  }

  _infoPerson(event) {
    console.log('_infoPerson');

    const { detail: { id }} = event;
    const index = this.people.findIndex(person => person.id === id);
    const chosenPersonCopy = { ...this.people[index] };

    const personaForm = this.shadowRoot.querySelector('persona-form');
    personaForm.person = chosenPersonCopy;

    this.showPersonForm = true;
  }

  _deletePerson(event) {
    console.log('_deletePerson');

    const { detail: { name, id }} = event;

    console.log(`Se va a eliminar el usuario ${name} con id: ${id}`);
    const index = this.people.findIndex(person => person.id === id);
    this.people = [...this.people.slice(0, index), ...this.people.slice(index + 1, this.people.length)]
    this.requestUpdate();
  }

  _personFormClose(event) {
    console.log('_personFormClose');

    this._closeForm();
  }

  _closeForm() {
    console.log('_closeForm');
    this.showPersonForm = false;
  }

  _filterPeople() {
    console.log('_filterPeople');

    this.filteredPeople = [ ...this.people.filter(person => person.yearsInCompany <= this.yearsRange) ];
    console.log('TEST', this.filteredPeople)
  }
}

  customElements.define(PersonaMain.is, PersonaMain);