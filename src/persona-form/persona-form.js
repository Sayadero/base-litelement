import { LitElement, html } from 'lit-element'
import { ifDefined } from 'lit-html/directives/if-defined'

class PersonaForm extends LitElement{
  static get is() {
    return 'persona-form';
  }

  static get properties() {
    return {
      person: { type: Object }
    }
  }

  constructor() {
    super();

    this.person = {
      name: '',
      yearsInCompany: 0,
      profile: ''
    };
  }

  updated(changedProperties) {
    console.log('Updated', changedProperties);
  }

  render() {
    return html`
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous"></link>
    <div>
      <form id="persona-form">
        <div class="form-group">
          <label>Nombre Completo</label>
          <input
            type="text"
            class="form-control"
            placeholder="Nombre Completo"
            value='${this.person.name}'
            @input=${({ target: { value }}) => this._updatePerson('name', value)}
            ?disabled="${this.person?.id}" />
        </div>
        <div class="form-group">
          <label>Perfil</label>
          <textarea
            class="form-control"
            placeholder="Perfil"
            rows="5"
            .value='${this.person.profile}'
            @input=${({ target: { value }}) => this._updatePerson('profile', value)}></textarea>
        </div>
        <div class="form-group">
          <label>Años en la empresa</label>
          <input
            type="text"
            class="form-control"
            placeholder="Años en la empresa"
            value='${this.person.yearsInCompany.toString()}'
            @input=${({ target: { value }}) => this._updatePerson('yearsInCompany', value)} />
        </div>
        <button class="btn btn-primary" @click=${this._goBack}><strong>Atrás</strong></button>
        <button class="btn btn-success" @click=${this._storePerson}><strong>Guardar</strong></button>
      </form>
    </div>
    `;
  }

  _goBack(event) {
    console.log('_goBack');
    event.preventDefault();

    this.dispatchEvent(new CustomEvent('persona-form-close', {
      bubbles: true,
      composed: true
    }));

    this._resetForm();
  }

  _storePerson(event) {
    console.log('_storePerson');
    event.preventDefault();

    console.log('Guardando la persona', this.person)

    if (this.person?.id) {
      this.dispatchEvent(new CustomEvent('edit-person', {
        bubbles: true,
        composed: true,
        detail: {
          person: { ...this.person }
        }
      }));
    } else {
      this.dispatchEvent(new CustomEvent('store-person', {
        bubbles: true,
        composed: true,
        detail: { ...this.person }
      }));
    }

    this._resetForm();
  }

  _updatePerson(property, value) {
    console.log('_updatePerson');

    console.log(`Actualizando la propiedad ${property} con el valor: ${value}`)

    this.person[property] = value;
  }

  _resetForm() {
    console.log('_resetForm', this.person);

    this.person = {
      name: '',
      yearsInCompany: 0,
      profile: ''
    };

    this.shadowRoot.querySelector('#persona-form').reset();
  }
}

customElements.define(PersonaForm.is, PersonaForm);