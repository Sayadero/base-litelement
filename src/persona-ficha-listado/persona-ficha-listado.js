import { LitElement, html } from 'lit-element'

class PersonaFichaListado extends LitElement{
  static get is() {
    return 'persona-ficha-listado';
  }

  static get properties() {
    return {
      id: { type: Number },
      fName: { type: String },
      yearsInCompany: { type: Number },
      profile: {type: String },
      photo: { type: Object }
    }
  }

  constructor() {
    super();
  }

  updated(changedProperties) {
    console.log('Updated', changedProperties);
  }

  render() {
    return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous"></link>
      <div class="card h-100">
        <div>
          <img class="card-img-top" src="${this.photo.src}" alt="${this.photo.alt}" height="300" width="250">
        </div>
        <div class="card-body">
          <h5 class="card-title">${this.fName}</h5>
          <p class="card-text">${this.profile}</p>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">${this.yearsInCompany} años en la empresa</li>
          </ul>
        </div>
        <div class="card-footer">
          <button class="btn btn-danger col-5" @click=${this._deletePerson}><strong>X</strong></button>
          <button class="btn btn-info col-5 offset-1" @click="${this._infoPerson}"><strong>Info</strong></button>
        </div>
      </div>
    `;
  }

  _infoPerson(event) {
    console.log('_infoPerson');

    this.dispatchEvent(new CustomEvent('info-person', {
      bubbles: true,
      composed: true,
      detail: {
        id: this.id
      }
    }));
  }

  _deletePerson(event) {
    console.log('_deletePerson');
    console.log(`Se va a eliminar el usuario: ${this.fName}`);

    this.dispatchEvent(new CustomEvent('delete-person', {
      bubbles: true,
      composed: true,
      detail: {
        id: this.id,
        name: this.fName
      }
    }));
  }
}

customElements.define(PersonaFichaListado.is, PersonaFichaListado);