import { LitElement, html } from 'lit-element'

class EmisorEvento extends LitElement{
    static get is() {
        return 'emisor-evento';
    }

    static get properties() {
        return {
        }
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <h3>Emisor Evento</h3>
            <button @click="${this.sendEvent}">No Pulsar</button>
        `;
    }

    sendEvent(event) {
        console.log("sendEvent");
        console.log(event);

        this.dispatchEvent(new CustomEvent('test-event', {
            bubbles: true,
            composed: true,
            detail: {
                course: "TechU",
                year: 2021
            }
        }))
    }
}

customElements.define(EmisorEvento.is, EmisorEvento);