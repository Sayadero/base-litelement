import { LitElement, html } from 'lit-element'
import '../emisor-evento/emisor-evento.js'
import '../receptor-evento/receptor-evento.js'

class GestorEvento extends LitElement{
    static get is() {
        return 'gestor-evento';
    }

    static get properties() {
        return {
        }
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <h1>Gestor Evento</h1>
            <emisor-evento @test-event=${this.handleTestEvent}></emisor-evento>
            <receptor-evento id="receptorEvento"></receptor-evento>
        `;
    }

    handleTestEvent(event) {
        console.log('handleTestEvent');
        console.log(event);

        const { detail: { course, year } } = event;

        const receptorEventoElement = this.shadowRoot.querySelector('#receptorEvento');
        receptorEventoElement.course = course;
        receptorEventoElement.year = year;
    }
}

customElements.define(GestorEvento.is, GestorEvento);