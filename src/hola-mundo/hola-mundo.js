import { LitElement, html } from 'lit-element'

class HolaMundo extends LitElement{
    static get is() {
        return 'hola-mundo';
    }

    render() {
        return html`
            <div>Hola Mundo!</div>
        `;
    }
}

customElements.define(HolaMundo.is, HolaMundo);