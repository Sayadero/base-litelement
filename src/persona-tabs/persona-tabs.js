import { LitElement, html } from 'lit-element'
import '../test-api/test-api.js'
import '../gestor-evento/gestor-evento.js'
import '../hola-mundo/hola-mundo.js'
import '../ficha-persona/ficha-persona.js'
import '../test-bootstrap/test-bootstrap.js'
import '../persona-sidebar/persona-sidebar.js'
import '../persona-main/persona-main.js'

class PersonaTabs extends LitElement{
  static get is() {
    return 'persona-tabs';
  }

  static get properties() {
    return {
      activeTab: { type: Number },
      peopleStats: { type: Object },
      filteredPeopleStats: { type: Object },
      yearsRange: { type: Number }
    }
  }

  constructor() {
    super();

    this.activeTab = 0;
    this._tabsConfig = [
      {
        tabName: 'Aplicación',
        tabContentFunc: () => html`
          <div class="row">
            <persona-sidebar
              class="col-2"
              .peopleStats="${this.peopleStats}"
              .filteredPeopleStats="${this.filteredPeopleStats}"
              @new-person=${this._newPerson}
              @years-range-changed=${this._yearsRangeFilter}>
            </persona-sidebar>
            <persona-main
              class="col-10"
              years-range="${this.yearsRange}">
            </persona-main>
          </div>`
      },
      {
        tabName: 'Test API',
        tabContentFunc: () => html`<test-api></test-api>`
      },
      {
        tabName: 'Gestor Evento',
        tabContentFunc: () => html`<gestor-evento></gestor-evento>`
      },
      {
        tabName: 'Hola Mundo',
        tabContentFunc: () => html`<hola-mundo></hola-mundo><ficha-persona></ficha-persona>`
      },
      {
        tabName: 'Bootstrap Testing',
        tabContentFunc: () => html`<test-bootstrap></test-bootstrap>`
      }
    ];
    this.peopleStats = {};
    this.filteredPeopleStats = {};
    this.yearsRange = 0;
  }

  updated(changedProperties) {
    console.log('Updated', changedProperties);
  }

  render() {
    return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous"></link>
      <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
        ${this._tabsConfig.map(({ tabName }, tabIndex) => html`
          <li class="nav-item" role="presentation">
            <button
              class="nav-link ${this.activeTab === tabIndex ? 'active' : ''}"
              type="button"
              @click=${() => this.activeTab = tabIndex}">
              ${tabName}
            </button>
          </li>
        `)}
      </ul>
      <div class="tab-content" id="pills-tabContent">
        ${this._tabsConfig.map(({ tabContentFunc }, tabIndex) => html`
          <div class="tab-pane fade ${this.activeTab === tabIndex ? 'show active' : ''}">
            ${tabContentFunc()}
          </div>
        `)}
      </div>
    `;
  }

  _newPerson(event) {
    console.log('_newPerson');

    this.shadowRoot.querySelector('persona-main').showPersonForm = true;
  }

  _yearsRangeFilter(event) {
    console.log('_yearsRangeFilter');
    const { detail } = event;

    this.yearsRange = detail;
  }
}

customElements.define(PersonaTabs.is, PersonaTabs);