import { LitElement, html, css } from 'lit-element'

class PersonaSidebar extends LitElement{
  static get is() {
    return 'persona-sidebar';
  }

  static get properties() {
    return {
      peopleStats: { type: Object },
      filteredPeopleStats: { type: Object },
      yearsRange: { type: Number }
    }
  }

  static get styles() {
    return css`
      .margin-left {
        margin-left: 0.5rem;
      }

      .margin-right {
        margin-right: 0.5rem;
      }
    `;
  }

  constructor() {
    super();

    this.peopleStats = {};
    this.filteredPeopleStats = {};
    this.yearsRange = Number.MAX_SAFE_INTEGER;
  }

  updated(changedProperties) {
    console.log('Updated', changedProperties);

    if (changedProperties.has('peopleStats')) {
      if (this.yearsRange < this.peopleStats?.maxYearsInCompany) {
        this.yearsRange = this.peopleStats?.minYearsInCompany;
      } else if (this.yearsRange > this.peopleStats?.minYearsInCompany) {
        this.yearsRange = this.peopleStats?.maxYearsInCompany;
      }
    }

    if (changedProperties.has('yearsRange')) {
      this.dispatchEvent(new CustomEvent('years-range-changed', {
        bubbles: true,
        composed: true,
        detail: this.yearsRange
      }));
    }
  }

  render() {
    return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous"></link>
      <aside>
        <section>
          <div>
            Se muestran <span class="badge bg-pill bg-primary">${this.filteredPeopleStats?.numberOfPeople}</span> personas
            <br />
            Hay un total de <span class="badge bg-pill bg-primary">${this.peopleStats?.numberOfPeople}</span> personas
          </div>
          <div class="mt-5">
            <button
              class="w-100 btn bg-success"
              style="font-size: 50px"
              @click=${this._newPerson}>
              <strong>+</strong>
            </button>
            <div class="mt-3">
              <label for="yearsRange1" class="form-label">Años en la empresa</label>
              <div class="d-flex justify-content-center mt-1">
                <span class="mt-1 margin-right"><strong>${this.peopleStats?.minYearsInCompany}</strong></span>
                <input
                  id="yearsRange1"
                  class="form-range"
                  type="range"
                  min="${this.peopleStats?.minYearsInCompany}"
                  max="${this.peopleStats?.maxYearsInCompany}"
                  value="${this.yearsRange}"
                  @input=${this._yearsRangeChange} />
                <span class="mt-1 margin-left"><strong>${this.peopleStats?.maxYearsInCompany}</strong></span>
              </div>
              <div class="d-flex justify-content-center">
                <span class="badge bg-pill bg-primary"><strong>${this.yearsRange}</strong></span>
              </div>
            </div>
          </div>
        </section>
      </aside>
    `;
  }

  _newPerson(event) {
    console.log('_newPerson');
    console.log('Se va a crear una persona');

    this.dispatchEvent(new CustomEvent('new-person'), {
      bubbles: true,
      composed: true
    });
  }

  _yearsRangeChange(event) {
    console.log('_yearsRangeChange');

    const { target: { value }} = event;
    this.yearsRange = value;
  }
}

customElements.define(PersonaSidebar.is, PersonaSidebar);