import { LitElement, html } from 'lit-element'

class PersonaHeader extends LitElement{
    static get is() {
        return 'persona-header';
    }

    static get properties() {
        return {
        }
    }

    constructor() {
        super();
    }

    updated(changedProperties) {
        console.log('Updated', changedProperties);
    }

    render() {
        return html`
            <h1>Persona Header</h1>
        `;
    }
}

customElements.define(PersonaHeader.is, PersonaHeader);