import { LitElement, html } from 'lit-element'

class TestApi extends LitElement{
  static get is() {
    return 'test-api';
  }

  static get properties() {
    return {
      movies: { type: Array }
    }
  }

  constructor() {
    super();

    this.movies = [];
  }

  firstUpdated() {
    this._getMovies();
  }

  render() {
    return html`
      <h3>Test Api</h3>
      ${this.movies.map(movie => html`<div>La película <b>${movie.title}</b> está dirigida por <b>${movie.director}</b></div>`)}
    `;
  }

  sendEvent(event) {
    console.log("sendEvent");
    console.log(event);

    this.dispatchEvent(new CustomEvent('test-event', {
      bubbles: true,
      composed: true,
      detail: {
        course: "TechU",
        year: 2021
      }
    }))
  }

  async _getMovies() {
    const result = await fetch("https://swapi.dev/api/films");

    if (result.ok) {
      const jsonResult = await result.json();
      this.movies = jsonResult.results;
    } else {
      throw new Error(`HTTP ERR: ${result.status}`);
    }
  }
}

customElements.define(TestApi.is, TestApi);