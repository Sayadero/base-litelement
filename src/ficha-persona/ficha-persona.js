import { LitElement, html } from 'lit-element'

class FichaPersona extends LitElement{
    static get is() {
        return 'ficha-persona';
    }

    static get properties() {
        return {
            name: { type: String },
            yearsInCompany: { type: Number },
            personInfo: { type: String }
        }
    }

    constructor() {
        super();

        this.name = "Prueba Nombre";
        this.yearsInCompany = 12;
    }

    updated(changedProperties) {
        console.log('Updated', changedProperties);

        changedProperties.forEach((oldValue, propName) => {
            console.log(`Propiedad '${propName}' cambia valor, el valor anterior era: ${oldValue}`);
        });

        if (changedProperties.has('name')) {
            console.log(`Propiedad 'name' cambia valor, el valor anterio era: ${changedProperties.get('name')}. El nuevo valor es : ${this.name}`);
        }

        if (changedProperties.has('yearsInCompany')) {
            this._calculatePersonInfo();
        }
    }

    render() {
        return html`
            <div>
                <label>Nombre Completo</label>
                <input type="text" id="fname" name="fname" value="${this.name}" @change="${this.updateName}" />
                <br />
                <label>Años en la empresa</label>
                <input type="text" name="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}" />
                <br />
                <input type="text" value="${this.personInfo}" disabled />
            </div>
            `;
    }

    _calculatePersonInfo() {
        if (this.yearsInCompany >= 7) {
            this.personInfo = 'lead';
        }
        else if (this.yearsInCompany >= 5) {
            this.personInfo = 'senior';
        }
        else if (this.yearsInCompany >= 3) {
            this.personInfo = 'team';
        }
        else {
            this.personInfo = 'junior';
        }
    }

    updateName(event) {
        console.log('updateName', event);
        this.name = event.target.value;
    }

    updateYearsInCompany(event) {
        console.log('updateYearsInCompany', event);
        this.yearsInCompany = event.target.value;
    }
}

customElements.define(FichaPersona.is, FichaPersona);