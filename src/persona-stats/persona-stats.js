
import { LitElement, html } from 'lit-element'

class PersonaStats extends LitElement{
  static get is() {
    return 'persona-stats';
  }

  static get properties() {
    return {
      people: { type: Array },
      filteredPeople: { type: Array }
    }
  }

  constructor() {
    super();

    this.people = [];
    this.filteredPeople = [];
  }

  updated(changedProperties) {
    console.log('Updated', changedProperties);

    if (changedProperties.has('people')) {
      const peopleStats = this._gatherPeopleArrayInfo(this.people);
      this.dispatchEvent(new CustomEvent('people-stats-changed', {
        bubbles: true,
        composed: true,
        detail: {
          peopleStats
        }
      }))
    }

    if (changedProperties.has('filteredPeople')) {
      const filteredPeopleStats = this._gatherPeopleArrayInfo(this.filteredPeople);
      this.dispatchEvent(new CustomEvent('filtered-people-stats-changed', {
        bubbles: true,
        composed: true,
        detail: {
          filteredPeopleStats
        }
      }))
    }
  }

  render() {
    return html``;
  }

  _gatherPeopleArrayInfo(people = []) {
    console.log('_gatherPeopleArrayInfo', people);

    return {
      numberOfPeople: people.length,
      minYearsInCompany: people.reduce((minYearsInCompany, person) => minYearsInCompany > person.yearsInCompany ? person.yearsInCompany : minYearsInCompany, Number.MAX_SAFE_INTEGER),
      maxYearsInCompany: people.reduce((maxYearsInCompany, person) => maxYearsInCompany < person.yearsInCompany ? person.yearsInCompany : maxYearsInCompany, 0)
    };
  }
}

customElements.define(PersonaStats.is, PersonaStats);