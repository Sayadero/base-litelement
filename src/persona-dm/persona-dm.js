import { LitElement, html } from 'lit-element'

class PersonaDM extends LitElement{
  static get is() {
    return 'persona-dm';
  }

  static get properties() {
    return {}
  }

  constructor() {
    super();
  }

  firstUpdated() {
    super.firstUpdated();

    this.dispatchEvent(new CustomEvent('retrieve-people-start', {
      bubbles: true,
      composed: true
    }));

    setTimeout(() => this._retrievePeople(), (Math.random() * 3 + 2) * 1000);
  }

  updated(changedProperties) {
    console.log('Updated', changedProperties);
  }

  _retrievePeople() {
    console.log('_retrievePeople');

    const people = [
      {
        id: 0,
        name: 'Adrián',
        yearsInCompany: 4,
        profile: "Lorem ipsum dolor sit amet.",
        photo: {
          src: './img/AkuAku.gif',
          alt: 'Adrián'
        }
      },
      {
        id: 1,
        name: 'Daniel',
        yearsInCompany: 6,
        profile: "Lorem ipsum.",
        photo: {
          src: './img/Crash.gif',
          alt: 'Daniel'
        }
      },
      {
        id: 2,
        name: 'Daniel',
        yearsInCompany: 2,
        profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        photo: {
          src: './img/Daxter.gif',
          alt: 'Daniel'
        }
      },
      {
        id: 3,
        name: 'Ricardo',
        yearsInCompany: 10,
        profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        photo: {
          src: './img/Jack.gif',
          alt: 'Ricardo'
        }
      },
      {
        id: 4,
        name: 'Sergio',
        yearsInCompany: 80,
        profile: "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        photo: {
          src: './img/NeoCortex.gif',
          alt: 'Sergio'
        }
      },
    ];

    this.dispatchEvent(new CustomEvent('retrieve-people-ok', {
      bubbles: true,
      composed: true,
      detail: people
    }));
  }
}

customElements.define(PersonaDM.is, PersonaDM);